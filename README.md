#### Overview

NeuroBytes-v04 software rewritten in the Arduino IDE, with plans to add some more functionality. To distinguish it from the actual NeuroBytes project's software, I'll be referring to this code as "HalfaByte" should the need arise (code for v04 boards - 4 bits making up half of a byte).

#### Required

* ATtiny by damellis - https://github.com/damellis/attiny (board files)
* arduino-softpwm by Palatis - https://github.com/Palatis/arduino-softpwm (software PWM library)

USI send code taken from here (adapted for the ATtiny44 by me and split off into a separate lib by timemage):  
http://becomingmaker.com/usi-serial-send-attiny/
