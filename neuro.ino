#include <SoftPWM.h>
#include "src/USISerialSend/USISerialSend.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  
 * ATtiny44 pinout
 *                           +-\/-+
 *                     VCC  1|    |14  GND
 *             (D 10)  PB0  2|    |13  PA0  (D  0)        AREF
 *             (D  9)  PB1  3|    |12  PA1  (D  1) 
 *                     PB3  4|    |11  PA2  (D  2) 
 *  PWM  INT0  (D  8)  PB2  5|    |10  PA3  (D  3) 
 *  PWM        (D  7)  PA7  6|    |9   PA4  (D  4) 
 *  PWM        (D  6)  PA6  7|    |8   PA5  (D  5)        PWM
 *                           +----+
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * NeuroByte v04 pinout
 *  __________     _____________     __________     __________
 * | K5 K3 K1 |   | PA1 PA3 PA5 |   | 12 10  8 |   | D1 D3 D5 |
 * | K6 K4 K2 |   | PA0 PA2 PA4 |   | 13 11  9 |   | D0 D2 D4 |
 * |          |   |             |   |          |   |          |
 * |_K7_______|   |_PA6_________|   |__7_______|   |_D6_______|
 * 
 *   Headers           Ports          Physical       Arduino
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* mV values for important points in the membrane potential range
   used for declaration of arrays below. */
#define V_MIN -100
#define V_HALF_MR -85 // halfway point between min and rest
#define V_REFRACTORY -85
#define V_REST -70
#define V_HALF_RT -55 // halfway point between rest and thresh
#define V_THRESH -40
#define V_PEAK 55
#define INTERVAL 15
#define INTERVAL_NO 4

/* multipliers - see below. keep 'em in base 2 */
#define MULT_PWM 4
#define MULT_RES 32
//#define MULT_RES 1
/* used to convert between mem. potential values in mV
   and mem. potential values used in code */
#define COEFF_MV MULT_PWM/2*MULT_RES*INTERVAL_NO

#define ADD_V = abs(V_MIN)
/* number of PWM levels. */
#define PWM_LEVELS abs(V_MIN-V_THRESH)*MULT_PWM

/* indexes for important points in the membrane potential range
   for usage of arrays declared below. */
#define INDEX_MIN 0
#define INDEX_HALF_MR 1
#define INDEX_REFRACTORY 1
#define INDEX_REST 2
#define INDEX_HALF_RT 3
#define INDEX_THRESH 4
#define INDEX_PEAK 5

/* which LED color corresponds to which channel */
#define RED 0
#define GREEN 1
#define BLUE 2

/* these are the three LED output channels: */
SOFTPWM_DEFINE_CHANNEL_INVERT(0, DDRB, PORTB, PB1); // PB1, red
SOFTPWM_DEFINE_CHANNEL_INVERT(1, DDRB, PORTB, PB0); // PB0, green
SOFTPWM_DEFINE_CHANNEL_INVERT(2, DDRB, PORTB, PB2); // PB2, blue
SOFTPWM_DEFINE_OBJECT_WITH_PWM_LEVELS(3, PWM_LEVELS+1); // channels, levels

/* dendritic input pins set to 1 in this mask. to only read
   dendrite input pins from PINA, do PINA & DENDRITE_PINS_MASK */
#define DENDRITE_PINS_MASK 0b00011111

/* membrane potential values in mV
 * bit of an explanation:
 * | ...
 * | AP!
 * | AP!
 * |---------------- V_THRESH =  -40 mV
 * | INTERVAL
 * |------------ V_HALF_RT
 * | INTERVAL
 * |---------------- V_REST   =  -70 mV
 * | INTERVAL
 * |------------ V_HALF_MR
 * | INTERVAL
 *  ---------------- V_MIN    = -100 mV
 * 
 * set thresh, rest and min so that INTERVALs are equal!
 * 
 * we level the scale off so it starts at 0 and multiply by
 * MULT_PWM and INTERVAL_NO so that each INTERVAL is
 * PWM_LEVELS/2. this gives a v_current resolution of
 * 4*PWM_LEVELS/2, and facilitates calculation of LED PWM levels.
 * 
 * here's an array that holds these values:
 */
const uint16_t v_values[6] =
        { 0,   // INDEX_MIN, [0]
         (V_HALF_MR+abs(V_MIN)) * MULT_PWM/2 * MULT_RES * INTERVAL_NO,   // INDEX_HALF_MR, [1]
         (V_REST+abs(V_MIN))    * MULT_PWM/2 * MULT_RES * INTERVAL_NO,   // INDEX_REST, [2]
         (V_HALF_RT+abs(V_MIN)) * MULT_PWM/2 * MULT_RES * INTERVAL_NO,   // INDEX_HALF_RT, [3]
         (V_THRESH+abs(V_MIN))  * MULT_PWM/2 * MULT_RES * INTERVAL_NO,   // INDEX_THRESH, [4]
         (V_PEAK+abs(V_MIN))    * MULT_PWM/2 * MULT_RES * INTERVAL_NO }; // INDEX_PEAK, [5]
uint16_t v_current = v_values[INDEX_REST]; // current v = resting v
uint16_t v_target; // new resting potentatial based on dendritic input

/* whether or not to fire an AP */
uint8_t flag_fireap = 0;

/* 
 * dendrites
 * den_con_type, den_register: each bit carries each dendrite's value,
 * respectively, starting with LSB (with two MSBs left unused),
 * so corresponding PORTA pin names are: 0bXX543210
 * den_magnitudes: indexes correspond to PORTA pin names
 */
const uint8_t den_number = 5; // number of dendrites
const int16_t den_magnitudes[5] = { -20*COEFF_MV, 20*COEFF_MV, -20*COEFF_MV, 20*COEFF_MV, -20*COEFF_MV }; // input magnitudes in mV
uint8_t den_register = 0; // read dendrite input register PINA into this var with "& DENDRITE_PINS_MASK"
uint8_t flag_refractoriness = 0;
int8_t stimulus_edge = 0; // 1 when dendritic stimulus added, -1 when removed, 0 if unchanged
int16_t den_v_offset = 0; // by how much the dendrites in total offset the membrane potential
int16_t den_v_offset_newlyadded = 0; // membrane potential offset of the latest stimulus
volatile uint8_t den_register_volatile = 0; // ISR reads PINA into this var
volatile uint8_t flag_pinchange = 0; // 1 on pin change
uint8_t flag_newstimadded = 0;
unsigned long timer_ax = 0;
uint8_t ax_pulselen = 100;

/* read inputs, and save the membrane potential offset into den_v_offset */
int8_t den_refresh(void);

void axon_off(void);

/* outputs newV+den_v_offset, a new membrane potential value based on the
   current state of the dendrites */
uint16_t get_v_target(int8_t stim_e);

/* hold a membrane potential for some time.
   basically a delay, with serial output updates */
void holdV(void);

/* fire an action potential */
void fire_ap(void);

/* set a membrane potential to a new value.
   "curve" refers to serial output, rgb led value
   is always changed linearly. */
void setV_exp(uint16_t newV, uint8_t set_led=true, uint16_t delay_us=200); // exponential curve
void setV_log(uint16_t newV, uint8_t set_led=true, uint16_t delay_us=200, uint8_t sig=false); // logarithmic curve
void setV_sig(uint16_t newV, uint8_t set_led=true, uint16_t delay_us=200); // sigmoid curve

/* update the led based on the membrane potential */
void upd_led(uint16_t potential=v_current);

/* turn led off or on, white, full brightness */
void led_off(void);
void led_white(void);

/* send the membrane potential value over USI */
void upd_ser(uint16_t potential=v_current);

ISR (PCINT0_vect) {
    uint8_t tempregister = PINA;
    delayMicroseconds(100);
    if (PINA != tempregister) return; // debounce
    else {
        flag_pinchange = 1;
        den_register_volatile = tempregister & DENDRITE_PINS_MASK;
    }
}

void setup() {
    Palatis::SoftPWM.begin(50);
	
    cli();
    /* PA0-4 inputs, no pull-up (dendrites);
     * PA5 output, high (USI out); PA6 output, low (axon)
     * PA7 input, pull-up (unused) */
    DDRA  = 0b01100000;
    PORTA = 0b10100000;
    
    // enable pin change interrupts for dendrites (PA5 used for serial output)
    PCMSK0 = DENDRITE_PINS_MASK;
    GIMSK |= 1<<PCIE0;
    sei();
    
    // do a first read of the dendrites
    den_register_volatile = PINA & DENDRITE_PINS_MASK;
    den_refresh();
    upd_ser();
    upd_led();
    v_target = get_v_target(v_current);
    setV_log(v_target);
}

void loop() {
    upd_ser();
    upd_led();
    stimulus_edge = den_refresh();
    // if there's no new stimulus, or a stimulus stops
    if (stimulus_edge <= 0) { 
        /* hold the membrane potential for a bit, unless
           we're coming out of an AP. this prolongs
           EPSPs/IPSPs (new excitations/inhibitions) a bit */
        if (!flag_refractoriness) {
            holdV();
        } else {
            flag_refractoriness = 0;
        }
        /* if there was no pin change while holding, update v_current
           while tracing a sigmoid curve. otherwise, just go on */
        if (!flag_pinchange) {
            v_target = get_v_target(stimulus_edge);
            setV_sig(v_target, true, 1000);
        }
    // else if a new stimulus is added
    } else if (stimulus_edge > 0) {
        v_target = get_v_target(stimulus_edge);
        setV_log(v_target);
    }
    // flag_fireap gets set if V_THRESH gets reached. fire an AP if so.
    if (flag_fireap) fire_ap();
    delay(10);
}

int8_t den_refresh(void) {
    int16_t d_sum_total = 0, d_sum_newstim = 0;
    uint8_t pin;
    int8_t edge = 0;
    uint8_t changed_pins;
    uint8_t register_nonvolatile = den_register_volatile;
    flag_pinchange = 0;
    flag_newstimadded = 0;
    delayMicroseconds(100);
    // if input is unchanged, return 0
    //~ if (den_register_volatile == den_register) return edge;
    if (register_nonvolatile > den_register) edge = 1;
    else if (register_nonvolatile < den_register) edge = -1;
    
    // calculate how much offset was just added/removed
    changed_pins = register_nonvolatile ^ den_register;
    for (uint8_t i=0; i<den_number; ++i) {
        pin = changed_pins & (1 << i);
        if (pin) d_sum_newstim += den_magnitudes[i];
    }
    
    // calculate total offset from resting potential
    den_register = register_nonvolatile;
    for (uint8_t i=0; i<den_number; ++i) {
        pin = den_register & (1 << i);
        if (pin) d_sum_total += den_magnitudes[i];
    }
    
    den_v_offset = d_sum_total;
    den_v_offset_newlyadded = d_sum_newstim;
    return edge;
}

void axon_off() {
    if ( (PORTA & (1<<PA6)) && ((millis()-timer_ax) > ax_pulselen) ) {
        PORTA &= ~(1<<PA6);
    }
}
    

uint16_t get_v_target(int8_t stim_e) {
    /* 
     * returns a membrane potential value offset by
     * the current state of the dendritic inputs
     */
    uint16_t v_input;
    int16_t v_input_offset;
    /* if a new stimulus has just been added, add its value to the
     * current potential. this allows for "temporal summation" of stimuli.
     * 
     * otherwise, just add the total offsets of all dendrites
     * to the resting potential value.
     */
    if (stim_e == 1) {
        v_input = v_current;
        v_input_offset = den_v_offset_newlyadded;
    } else {
        v_input = v_values[INDEX_REST];
        v_input_offset = den_v_offset;
    }
    
    
    if (v_input_offset > 0) {
        v_input += v_input_offset;
        if (v_input > v_values[INDEX_THRESH]) {
            v_input = v_values[INDEX_THRESH];
        }
    } else {
        if (abs(v_input_offset) >= v_input) {
            v_input = 0;
        } else {
            v_input -= abs(v_input_offset);
        }
    }
    return v_input;
}

void holdV(void) {
    for (uint8_t i=0; i<100; ++i) {
        delay(30);
        upd_ser();
        upd_led();
        axon_off();
        if (flag_pinchange) return;
    }
}

void fire_ap(void) {
    uint16_t peak = v_values[INDEX_PEAK];
    uint16_t trough = v_values[flag_refractoriness];
    uint16_t x = 1;
    uint8_t add = 1;
    uint16_t udiff, qway; // diff to target V, and quarter of the way
    
    flag_fireap = 0;
    led_off();
    axon_off();
    
    udiff = (peak+2*COEFF_MV)-v_current;
    qway = v_current+(udiff/4);
    while (v_current < peak) {
        v_current += ((peak+2*COEFF_MV)-v_current)/2;
        if (v_current > peak) v_current = peak;
        upd_ser();
        axon_off();
        delay(20);
    }
    // downhill from here
    udiff = v_current-trough;
    qway = v_current-20*COEFF_MV; // 20 because I like the slope at this point
    x = COEFF_MV/2;
    led_white();
    while (v_current > qway) {
        v_current -= x;
        upd_ser();
        axon_off();
        x *= 2;
        delay(40);
        led_off();
    }
    x /= 2;
    while (v_current > v_values[INDEX_HALF_RT]) { // this one's completely arbitrary
        v_current -= x;
        upd_ser();
        axon_off();
        delay(40);
    }
    while (v_current > (trough+1*COEFF_MV)) {
        v_current -= (v_current-trough)/2;
        if (v_current < trough) v_current = trough;
        upd_ser();
        axon_off();
        delay(40);
    }
    //~ led_off();
    
    //~ led_white();
    //~ delay(50);
    //~ led_off();
    //~ upd_ser();
    //~ delay(150);
    
    upd_ser();
    upd_led();
    flag_refractoriness = 1;
    PORTA |= (1<<PA6);
    timer_ax = millis();
}

void setV_exp(uint16_t newV, uint8_t set_led=true, uint16_t delay_us=200) {
    uint16_t x = 1;
    uint8_t add = 1;
    uint16_t v_current_linstep = v_current;
    uint16_t udiff;
    uint16_t udiff_sh;
    uint8_t steps = 0;
    const uint8_t led_steps = 4;
    uint16_t lin_chunks;
    
    if (newV < v_current) {
        add = 0;
        udiff = v_current-newV;
    } else {
        udiff = newV-v_current;
    }
    
    if (udiff < led_steps) {
        v_current = newV;
        upd_ser();
        upd_led();
        axon_off();
        return;
    }
    
    udiff_sh = udiff/led_steps;
    
    while (udiff_sh != 0) {
        udiff_sh >>= 1;
        ++steps;
    }
    
    if (steps < 1) {
        v_current = newV;
        upd_ser();
        upd_led();
        axon_off();
        return;
    }
    
    lin_chunks = (udiff/led_steps)/(steps);
	
    if (flag_pinchange) return;
	for (uint8_t i=0; i<(steps); i++) {
		x *= 2;
        if (add) v_current += x;
        else v_current -= x;
        upd_ser();
        axon_off();

        for (uint16_t j=0; j<lin_chunks; ++j) {
            delayMicroseconds(delay_us);
            if (add) v_current_linstep += led_steps;
            else v_current_linstep -= led_steps;
            if (set_led) upd_led(v_current_linstep);
        }
        
        if (v_current >= (v_values[INDEX_THRESH]-2*COEFF_MV)) {
            flag_fireap = 1;
            return;
        }
        if (flag_pinchange) return;
        
    }
    v_current = newV;
    upd_ser();
    axon_off();
    if (set_led) upd_led();
}

void setV_log(uint16_t newV, uint8_t set_led=true, uint16_t delay_us=200, uint8_t sig=false) {
    uint16_t v_current_linstep = v_current;
	//~ int16_t diff = v_current-newV;
    uint8_t add = 1;
    uint16_t udiff;
    uint16_t udiff_sh;
    uint8_t steps = 0;
    const uint8_t led_steps = 4;
    uint16_t lin_chunks;
    
    if (newV < v_current) {
        add = 0;
        udiff = v_current-newV;
    } else {
        udiff = newV-v_current;
    }
    
    if (udiff < led_steps) {
        v_current = newV;
        upd_ser();
        upd_led();
        axon_off();
        return;
    }
    
    udiff_sh = udiff/led_steps;
    while (udiff_sh != 0) { 
        udiff_sh >>= 1;
        ++steps;
    }
    
    if (steps < 2) {
        v_current = newV;
        upd_ser();
        upd_led();
        axon_off();
        return;
    }
    lin_chunks = (udiff/led_steps)/(steps-1);
    if (sig) {
        for (uint16_t k=0; k<lin_chunks; ++k) {
            delayMicroseconds(delay_us);
        }
    }
    
    if (flag_pinchange) return;
	for (uint8_t i=0; i<(steps-1); i++) {
        if (add) {
            v_current += udiff/2;
            udiff = newV-v_current;
        } else {
            v_current -= udiff/2;
            udiff = v_current-newV;
        }
        
        // skip sending the first step if part of a sigmoid curve
		//~ if (!(sig && (i==0))) upd_ser();
        upd_ser();
        axon_off();
        
        // LED stuff
        for (uint16_t j=0; j<lin_chunks; ++j) {
            delayMicroseconds(delay_us);
            if (add) v_current_linstep += led_steps;
            else v_current_linstep -= led_steps;
            if (set_led) upd_led(v_current_linstep);
            axon_off();
        }
        
        if (v_current >= (v_values[INDEX_THRESH]-2*COEFF_MV)) {
            flag_fireap = 1;
            return;
        }
        if (flag_pinchange) return;
        
        // if the difference less than 0.25 mV, just round off
        //~ if (udiff < (MULT_PWM/2 * MULT_RES * INTERVAL_NO)/4) break;
	}
    v_current = newV;
    upd_ser();
    axon_off();
    if (set_led) upd_led();
}

void setV_sig(uint16_t newV, uint8_t set_led=true, uint16_t delay_us=200) {
    //~ uint16_t half_diff;
    if (!(v_current-newV)) return;
    if (newV < v_current) {
        //~ half_diff = (v_current-newV)/2;
        setV_exp(v_current-(v_current-newV)/2, set_led, delay_us);
    } else {
        //~ half_diff = (newV-v_current)/2;
        setV_exp(v_current+(newV-v_current)/2, set_led, delay_us);
    }
    if (flag_fireap || flag_pinchange) return;
    setV_log(newV, set_led, delay_us, true);
}

void upd_led(uint16_t potential=v_current) {
	/* 
	 * How LED levels change as V goes up from V_MIN to V_THRESH:
	 * 
	 *   ------------------------ V_THRESH
	 *  ^ R:50->100%  G:50->0%
	 *  ^------------------------ V_HALF_RT
	 *  ^ R:0->50%    G:100->50%
	 *  ^------------------------ V_REST
	 *  ^ G:50->100%  B:50->0%
	 *  ^------------------------ V_HALF_MR
	 *  ^ G:0->50%    B:100->50%
	 *  ^------------------------ V_MIN
	 * 
	 * look up exact values in v_values[] using
	 * INDEX_HALF_RT, INDEX_REST...
	 * 
	 */
	uint16_t level;
    uint8_t divider = 8;
    uint16_t max_level = PWM_LEVELS/divider;
	if (potential > (v_values[INDEX_HALF_RT])) {
		level = (potential - v_values[INDEX_HALF_RT])/MULT_RES/divider;
		Palatis::SoftPWM.set(RED, (max_level/2)+level);
		Palatis::SoftPWM.set(GREEN, (max_level/2)-level);
		Palatis::SoftPWM.set(BLUE, 0);
	}
	else if (potential > (v_values[INDEX_REST])) {
		level = (potential - v_values[INDEX_REST])/MULT_RES/divider;
		Palatis::SoftPWM.set(RED, level);
		Palatis::SoftPWM.set(GREEN, max_level-level);
		Palatis::SoftPWM.set(BLUE, 0);
	}
	else if (potential > (v_values[INDEX_HALF_MR])) {
		level = (potential - v_values[INDEX_HALF_MR])/MULT_RES/divider;
		Palatis::SoftPWM.set(RED, 0);
		Palatis::SoftPWM.set(GREEN, (max_level/2)+level);
		Palatis::SoftPWM.set(BLUE, (max_level/2)-level);
	}
	else {
		level = potential/MULT_RES/divider;
		Palatis::SoftPWM.set(RED, 0);
		Palatis::SoftPWM.set(GREEN, level);
		Palatis::SoftPWM.set(BLUE, max_level-level);
	}
}

void led_off(void) {
    for (uint8_t i=0; i<3; ++i) Palatis::SoftPWM.set(i, 0);
}

void led_white(void) {
    for (uint8_t i=0; i<3; ++i) Palatis::SoftPWM.set(i, 255);
}

void upd_ser(uint16_t potential=v_current) {
    char str_out[6];
    utoa(potential, str_out, 10);
    //~ for (size_t i=0; i<(5-strlen(str_out)); ++i) {
        //~ usiserial_send_byte('0');
    //~ }
    for (size_t i=0; str_out[i]; ++i) {
        usiserial_send_byte(str_out[i]);
    }
    usiserial_send_byte('\n');
    while (!usiserial_send_available());
}
